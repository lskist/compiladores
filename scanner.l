/* Compiladores, Turma B, Grupo:  Andre Luz Gondalves, Andre Santalo, Lisardo Sallaberry Kist */
%{
#include "comp_dict.h"
#include "tokens.h"
#include <stdlib.h>
#include <stdio.h>
#define IKS_SIMBOLO_INDEFINIDO 0
#define IKS_SIMBOLO_LITERAL_INT 1
#define IKS_SIMBOLO_LITERAL_FLOAT 2
#define IKS_SIMBOLO_LITERAL_CHAR 3
#define IKS_SIMBOLO_LITERAL_STRING 4
#define IKS_SIMBOLO_LITERAL_BOOL 5
#define IKS_SIMBOLO_IDENTIFICADOR 6

int running;
int dummy_type;
int dummy_line;
int dummy_key;

/* Essa são as funções de manipulação de String */
#define STRINGBUF_CHUNK 16
char *stringbuf = NULL;
int stringbuf_pos;
int stringbuf_size;
comp_dict_t* symbol_table;

void printSymbolTable() {
    print_dict(symbol_table);
}

/* Reseta */
void stringbuf_reset(void) {
	// free(stringbuf);
	stringbuf = NULL;
	stringbuf_pos = 0;
	stringbuf_size = 0;
}

/* Põe um caracter */
void stringbuf_push(char ch) {
	if (stringbuf_size == stringbuf_pos)
		stringbuf = realloc(stringbuf, stringbuf_size += STRINGBUF_CHUNK);
	stringbuf[stringbuf_pos++] = ch;
}

/* Macro para adição de tokens. Devia estar indo para uma tabela de simbolos */
#define add_token(lextype, lexkey, lexvalue) do { \
	char* str; \
	str = (char*)malloc(10*sizeof(char)); \
	sprintf(str, "%d", current_line); \
	symbol_table = dict_insert (symbol_table, lexkey, str); \
} while (0)

%}

%x COMMENT
%x STRING

%%
int     { return TK_PR_INT; }
float   { return TK_PR_FLOAT; }
bool    { return TK_PR_BOOL; }
char	{ return TK_PR_CHAR; }
string  { return TK_PR_STRING; }
if      { return TK_PR_IF; }
then    { return TK_PR_THEN; }  
else    { return TK_PR_ELSE; }
while   { return TK_PR_WHILE; }
do      { return TK_PR_DO; }
input   { return TK_PR_INPUT; }
output  { return TK_PR_OUTPUT; }
return  { return TK_PR_RETURN; }

" "     {}
"\t"    {}
"\n"    { current_line++; }

"//".*        {}
"/*"          { BEGIN(COMMENT); }
<COMMENT>"*/" { BEGIN(INITIAL); }
<COMMENT>"\n" { current_line++; }
<COMMENT>.    {}

[][<=>+*/%&$(){},;:!-] { return yytext[0]; }
"=="    { return TK_OC_EQ; }
"!="    { return TK_OC_NE; }
"<="    { return TK_OC_LE; }
">="    { return TK_OC_GE; }
"&&"    { return TK_OC_AND;}
"||"    { return TK_OC_OR; }



-?[0-9]+   {
	add_token(IKS_SIMBOLO_LITERAL_INT, yytext, atoi(yytext)); return TK_LIT_INT;
}

true    { add_token(IKS_SIMBOLO_LITERAL_BOOL, yytext, 1); return TK_LIT_TRUE; }
false   { add_token(IKS_SIMBOLO_LITERAL_BOOL, yytext, 0); return TK_LIT_FALSE; }

-?[0-9]+("."[0-9]+)?([eE][+-]?[0-9]+)? {
	add_token(IKS_SIMBOLO_LITERAL_FLOAT, yytext, atof(yytext)); return TK_LIT_FLOAT;
}

"\"" { 
	stringbuf_reset();
	BEGIN(STRING);
	return TK_LIT_STRING;
}

<STRING>[^\\\n"] { stringbuf_push(yytext[0]); }
<STRING>"\n"  { stringbuf_push('\n'); current_line++; }
<STRING>"\""  {
	BEGIN(INITIAL);
	add_token(IKS_SIMBOLO_LITERAL_STRING, stringbuf, stringbuf); 
}

<STRING>"\\". {
	int ch = yytext[1];
	if (ch == TOKEN_ERRO) {
		printf("Unknown sequence\n");
		return TOKEN_ERRO;
	}
	else
		stringbuf_push(ch);
}


\'.\'   { add_token(IKS_SIMBOLO_LITERAL_CHAR, yytext, yytext[1]); return TK_LIT_CHAR; }



[A-Za-z_][A-Za-z_0-9]* {
	add_token(IKS_SIMBOLO_IDENTIFICADOR, yytext, 0); return TK_IDENTIFICADOR;
}

.	{ printf("Found '%s'. I don't know that to do with that! Take it away!\n", yytext); return TOKEN_ERRO; }
%%

void initMe(void) {
	running = 1;
	current_line = 1;
	symbol_table = dict_create();
}

int yywrap(void) {
	running = 0;
	return 1;
}
