/* Compiladores, Turma B, Grupo: André Luz Gonçalves, André Santaló de Oliveira, Lisardo Sallaberry Kist */

#include <stdio.h>
#include <assert.h>
#include "comp_dict.h"
#include "comp_list.h"
#include "comp_tree.h"
#include "comp_graph.h"
#include "tokens.h"
#include "gv.h"

#define TEST_SIZE 5

int getLineNumber (void)
{
  /* deve ser implementada */
  return current_line;
}

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s\n", mensagem);
}

int main (int argc, char **argv)
{
  gv_init("./olha.dot");
  int resultado = yyparse();
  if(resultado)
	  printf("Erro encontrado na linha %d\n", current_line);
  gv_close();
  return resultado;
}

