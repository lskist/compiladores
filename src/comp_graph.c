/* Compiladores, Turma B, Grupo: André Luz Gonçalves, André Santaló de Oliveira, Lisardo Sallaberry Kist */
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include "comp_graph.h"

// Representa um grafo
struct graph {
    int n;              // Número de vértices //
    int m;              // Número de arestas //
    struct successors { // Lista de vértices //
        int d;            // Número de sucessores do vértice //
        int len;          // Número de slots no array list //
        char is_sorted;   // Se a lista de sucessores está ordenada ou năo //
        int list[1];      // Lista de sucessores desse vértice //
    } *alist[1];
};

// Cria um grafo do zero
// Entrada: número de vértices
// Saída: grafo
comp_graph_t graph_create(int n)
{
    comp_graph_t g;
    g = malloc(sizeof(struct graph) + sizeof(struct successors *) * (n-1));
    assert(g);

    g->n = n;
    g->m = 0;

    int i;
    for(i = 0; i < n; i++) {
        g->alist[i] = malloc(sizeof(struct successors));
        assert(g->alist[i]);

        g->alist[i]->d = 0;
        g->alist[i]->len = 1;
        g->alist[i]->is_sorted= 1;
    }
    
    return g;
}

// Dado um grafo, dá um "free" em todo seu conteúdo
// Entrada: grafo
void graph_destroy(comp_graph_t g)
{
    int i;

    for(i = 0; i < g->n; i++) free(g->alist[i]);
    free(g);
}

// Imprime o estado atual do grafo
// Entrada: grafo
void graph_print(comp_graph_t g)
{
    int i, j;

    for(i = 0; i < g->n; i++) 
    {
        printf("Nodo: %d: Conectado a %d nodos:", i, g->alist[i]->d);
        for(j = 0; j < g->alist[i]->d; j++)
            printf(" %d", g->alist[i]->list[j]);
        printf("\n");
    }

}

// Adiciona uma aresta entre 2 vértices
// Entrada: grafo, vértice com **menor** índice, vértice com **maior** índice
void graph_add_edge(comp_graph_t g, int u, int v)
{
    assert(u >= 0);
    assert(u < g->n);
    assert(v >= 0);
    assert(v < g->n);

    while(g->alist[u]->d >= g->alist[u]->len) {
        g->alist[u]->len *= 2;
        g->alist[u] =
            realloc(g->alist[u], 
                sizeof(struct successors) + sizeof(int) * (g->alist[u]->len - 1));
    }
    g->alist[u]->list[g->alist[u]->d++] = v;
    g->alist[u]->is_sorted = 0;
    g->m++;
}

// Adiciona um nodo
// Entrada: grafo
// Saída: grafo com novo nodo
comp_graph_t graph_add_node(comp_graph_t g) {
    int newnode = g->n;
    g = realloc(g, sizeof(struct graph) + sizeof(struct successors *) * (newnode));
    g->alist[newnode] = malloc(sizeof(struct successors));
    g->alist[newnode]->d = 0;
    g->alist[newnode]->len = 1;
    g->alist[newnode]->is_sorted= 1;
    g->n = newnode+1;
    return g;
}

// Funçăo "privada"
int graph_out_degree(comp_graph_t g, int source)
{
    assert(source >= 0);
    assert(source < g->n);
    return g->alist[source]->d;
}

// Funçăo "privada"
static int intcmp(const void *a, const void *b)
{
    return *((const int *) a) - *((const int *) b);
}

// Testa se o grafo possui uma aresta entre dois vértices
// Entrada: grafo, vértice com **menor** índice, vértice com **maior** índice
int graph_has_edge(comp_graph_t g, int u, int v)
{
    int i;

    assert(u >= 0);
    assert(u < g->n);
    assert(v >= 0);
    assert(v < g->n);

    if(graph_out_degree(g, u) >= 20) {
        // Ordena os vértices
        if(! g->alist[u]->is_sorted) {
            qsort(g->alist[u]->list,
                    g->alist[u]->d,
                    sizeof(int),
                    intcmp);
        }
        
        return 
            bsearch(&v,
                    g->alist[u]->list,
                    g->alist[u]->d,
                    sizeof(int),
                    intcmp)
            != 0;
    } else {
        for(i = 0; i < g->alist[u]->d; i++) {
            if(g->alist[u]->list[i] == v) return 1;
        }
        return 0;
    }
}

void tests_graph () {
     // Testa de criaçăo do grafo:
     comp_graph_t grafo = graph_create(5);
     
     // Adiciona algumas arestas:
     graph_add_edge(grafo,2,3);
     graph_add_edge(grafo,1,3);
     graph_add_edge(grafo,0,4);
     
     // Testa se possui arestas aleatórias:
     //// Devem retornar falso (0):
     printf("Teste de arestas que nao existem\n");
     printf("%i\n",graph_has_edge(grafo,1,2));
     printf("%i\n",graph_has_edge(grafo,0,2));
     printf("%i\n",graph_has_edge(grafo,1,4));
     //// Devem retornar verdadeiro (1):
     printf("Teste de arestas que existem\n");
     printf("%i\n",graph_has_edge(grafo,2,3));
     printf("%i\n",graph_has_edge(grafo,1,3));
     printf("%i\n",graph_has_edge(grafo,0,4));
     
     // Adiciona mais dois vértices
     grafo = graph_add_node(grafo);
     grafo = graph_add_node(grafo);
     
     // Adiciona mais duas arestas, usando os vértices novos
     graph_add_edge(grafo,3,6);    
     graph_add_edge(grafo,5,6);    
     
     // Testa se possui arestas novas:
     //// Devem retornar falso (0):
     printf("Teste de arestas que nao existem\n");
     printf("%i\n",graph_has_edge(grafo,0,1));
     printf("%i\n",graph_has_edge(grafo,2,5));
     //// Devem retornar verdadeiro (1):
     printf("Teste de arestas que existem\n");
     printf("%i\n",graph_has_edge(grafo,3,6));
     printf("%i\n",graph_has_edge(grafo,5,6));
     
     system("pause");
}

//int main () { tests_graph (); }

