/* Compiladores, Turma B, Grupo: Andr� Luz Gon�alves, Andr� Santal� de Oliveira, Lisardo Sallaberry Kist */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp_tree.h"
#include "iks_ast.h"
#include "gv.h"



// Cria um novo nodo com um dado especificado.
// Entrada: dado
// Sa�da: nodo com o dado
comp_tree_t* createNode(int data) {
    comp_tree_t* newNode = (comp_tree_t*)malloc(sizeof(comp_tree_t));
    //const void *gvNode = (const void*)malloc(sizeof(const void));
    newNode->youngerSibling = NULL;
    newNode->olderSibling = NULL;
    newNode->youngestChild = NULL;
    newNode->father = NULL;
    newNode->type = data;
    /*switch(data){
    case IKS_AST_IDENTIFICADOR:
        gv_declare(data, gvNode, "var ident");
        break;
    case IKS_AST_LITERAL:
        gv_declare(data, gvNode, "var lit");
        break;
    case IKS_AST_FUNCAO:
        gv_declare(data, gvNode, "var func");
        break;
    default:
        gv_declare(data, gvNode, NULL);
        break;
    }*/
    return newNode;
}


void typeToString(int type) {
    if( type == IKS_AST_PROGRAMA )
        printf("IKS_AST_PROGRAMA\n");
    if( type == IKS_AST_FUNCAO )
        printf("IKS_AST_FUNCAO\n");
    if( type == IKS_AST_IF_ELSE )
        printf("IKS_AST_IF_ELSE\n");
    if( type == IKS_AST_DO_WHILE )
        printf("IKS_AST_DO_WHILE\n");
    if( type == IKS_AST_WHILE_DO )
        printf("IKS_AST_WHILE_DO\n");
    if( type == IKS_AST_INPUT )
        printf("IKS_AST_INPUT\n");
    if( type == IKS_AST_OUTPUT )
        printf("IKS_AST_OUTPUT\n");
    if( type == IKS_AST_ATRIBUICAO )
        printf("IKS_AST_ATRIBUICAO\n");
    if( type == IKS_AST_RETURN )
        printf("IKS_AST_RETURN\n");
    if( type == IKS_AST_BLOCO )
        printf("IKS_AST_BLOCO \n");
    if( type == IKS_AST_IDENTIFICADOR )
        printf("IKS_AST_IDENTI\n");
    if( type == IKS_AST_LITERAL )
        printf("IKS_AST_LITERA\n");
    if( type == IKS_AST_ARIM_SOMA )
        printf("IKS_AST_ARIM_SOMA\n");
    if( type == IKS_AST_ARIM_SUBTRACAO )
        printf("IKS_AST_ARIM_SUBTRACAO\n");
    if( type == IKS_AST_ARIM_MULTIPLICACAO)
        printf("IKS_AST_ARIM_MULTIPLICACAO\n");
    if( type == IKS_AST_ARIM_DIVISAO    )
        printf("IKS_AST_ARIM_DIVISAO\n");
    if( type == IKS_AST_ARIM_INVERSAO )
        printf("IKS_AST_ARIM_INVERSAO\n");
    if( type == IKS_AST_LOGICO_E  )
        printf("IKS_AST_LOGICO_E\n");
    if( type == IKS_AST_LOGICO_OU  )
        printf("IKS_AST_LOGICO_OU\n");
    if( type == IKS_AST_LOGICO_COMP_DIF  )
        printf("IKS_AST_LOGICO_COMP_DIF\n");
    if( type == IKS_AST_LOGICO_COMP_IGUAL   )
        printf("IKS_AST_LOGICO_COMP_IGUAL\n");
    if( type == IKS_AST_LOGICO_COMP_LE      )
        printf("IKS_AST_LOGICO_COMP_LE\n");
    if( type == IKS_AST_LOGICO_COMP_GE)
        printf("IKS_AST_LOGICO_COMP_GE\n");
    if( type == IKS_AST_LOGICO_COMP_L )
        printf("IKS_AST_LOGICO_COMP_L\n");
    if( type == IKS_AST_LOGICO_COMP_G )
        printf("IKS_AST_LOGICO_COMP_G\n");
    if( type == IKS_AST_LOGICO_COMP_NEGACAO )
        printf("IKS_AST_LOGICO_COMP_NEGACAO \n");
    if( type == IKS_AST_VETOR_INDEXADO )
        printf("IKS_AST_VETOR_INDEXADO\n");
    if( type == IKS_AST_CHAMADA_DE_FUNCAO )
        printf("IKS_AST_CHAMADA_DE_FUNCAO\n");
    /*if(type == TK_PR_INT )
	    printf("TK_PR_INT\n");
    if(type == TK_PR_FLOAT )
	    printf("TK_PR_FLOAT\n");
    if(type == TK_PR_BOOL )
	    printf("TK_PR_BOOL\n");
    if(type == TK_PR_CHAR )
	    printf("TK_PR_CHAR\n");
    if(type == TK_PR_STRING )
	    printf("TK_PR_STRING\n");
    if(type == TK_PR_IF )
	    printf("TK_PR_IF\n");
    if(type == TK_PR_THEN )
	    printf("TK_PR_THEN\n");
    if(type == TK_PR_ELSE )
	    printf("TK_PR_ELSE\n");
    if(type == TK_PR_WHILE )
	    printf("TK_PR_WHILE\n");
    if(type == TK_PR_DO )
	    printf("TK_PR_DO\n");
    if(type == TK_PR_INPUT )
	    printf("TK_PR_INPUT\n");
    if(type == TK_PR_OUTPUT )
	    printf("TK_PR_OUTPUT\n");
    if(type == TK_PR_RETURN )
	    printf("TK_PR_RETURN\n");
    if(type == TK_OC_LE )
	    printf("TK_OC_LE\n");
    if(type == TK_OC_GE )
	    printf("TK_OC_GE\n");
    if(type == TK_OC_EQ )
	    printf("TK_OC_EQ\n");
    if(type == TK_OC_NE )
	    printf("TK_OC_NE\n");
    if(type == TK_OC_AND )
	    printf("TK_OC_AND\n");
    if(type == TK_OC_OR )
	    printf("TK_OC_OR\n");
    if(type == TK_LIT_INT )
	    printf("TK_LIT_INT\n");
    if(type == TK_LIT_FLOAT )
	    printf("TK_LIT_FLOAT\n");
    if(type == TK_LIT_FALSE )
	    printf("TK_LIT_FALSE\n");
    if(type == TK_LIT_TRUE )
	    printf("TK_LIT_TRUE\n");
    if(type == TK_LIT_CHAR )
	    printf("TK_LIT_CHAR\n");
    if(type == TK_LIT_STRING )
	    printf("TK_LIT_STRING\n");
    if(type == TK_IDENTIFICADOR )
	    printf("TK_IDENTIFICADOR\n");
    if(type == TOKEN_ERRO )
	    printf("TOKEN_ERRO\n"); */
}

const void* createGVT(comp_tree_t* node)
{
	const void* newGVNode;
	comp_tree_t* p;
	if(node == NULL)
		return NULL;
	const void *gvNode = (const void*)malloc(sizeof(const void));
    switch(node->type){
    case IKS_AST_IDENTIFICADOR:
        gv_declare(node->type, gvNode, "var ident");
        break;
    case IKS_AST_LITERAL:
        gv_declare(node->type, gvNode, "var lit");
        break;
    case IKS_AST_FUNCAO:
        gv_declare(node->type, gvNode, "var func");
        break;
    default:
        gv_declare(node->type, gvNode, NULL);
        break;
    }
    
    for (p = node->youngestChild; p != NULL; p = p->olderSibling) {
        newGVNode = createGVT(p);
        gv_connect(gvNode, newGVNode);
	}
    return gvNode;    
}


void printPretty(int depth, comp_tree_t* node)
{
    comp_tree_t* p;
    int i;
    for(i = depth; i > 0; i--)
        printf("  ");
    if(node->olderSibling == NULL)
        printf("\\-");
    else
        printf("|-");
    typeToString(node->type);   
    depth++;
    for (p = node->youngestChild; p != NULL; p = p->olderSibling) 
        printPretty(depth, p);
}

void printTree(comp_tree_t* node) {
    printPretty(0, node); 
}
//futuros comentarios
comp_tree_t* astCreate(int type, comp_tree_t* first, comp_tree_t* second, comp_tree_t* third, comp_tree_t* fourth) {
    comp_tree_t* current;
    current = createNode(type);
    if(first != NULL) {
        appendChild(current,first);  
    }
    if(second != NULL) {
        appendChild(current,second); 
    }
    if(third != NULL) {
        appendChild(current,third);
    }
    if(fourth != NULL) {
        appendChild(current,fourth);
    }
    return current;
}

// Torna um nodo pai de um outro nodo espec�fico, j� inicializado. Esse novo
// pai se torna av� dos filhos do outro nodo, e assim por diante.
// O nodo filho � desvencilhado seguramente (mantendo consist�ncia com pais
// e irm�os) de sua posi��o atual na �rvore antes de ser ligado ao novo pai.
// Entrada: nodo a se tornar pai, nodo a se tornar filho
void appendChild(comp_tree_t* newFather, comp_tree_t* newChild) {
    safelyRemoveNode(newChild);
    comp_tree_t* youngestChild_before = newFather->youngestChild;    
    newFather->youngestChild = newChild;
    newChild->father = newFather;
    newChild->youngerSibling = NULL;
    if (youngestChild_before == NULL) {        
        newChild->olderSibling = NULL;
    }
    else {
        newChild->olderSibling = youngestChild_before;
        youngestChild_before->youngerSibling = newChild;
    }
}

// Remove um nodo de uma �rvore de maneira segura (garantindo consist�ncia dos
// pais e irm�os) e descarta os filhos.
// Entrada: nodo a ser removido.
void safelyRemoveNode_DiscardingChildren(comp_tree_t* removableNode) {
    safelyRemoveNode(removableNode);
    freeNodeAndChildren(removableNode);
}

// Remove um nodo de uma �rvore de maneira segura (garantindo consist�ncia dos
// pais e irm�os) e liga os filhos �rf�os ao pai do nodo removido.
// Entrada: nodo a ser removido.
// Sa�da: o primeiro (mais novo) filho, caso exista, do nodo removido. Sen�o,
//        NULL.
comp_tree_t* safelyRemoveNode_AttachingChildrenToFather(comp_tree_t* removableNode) {
    comp_tree_t* youngestChild = removableNode->youngestChild;
    if (removableNode->father == NULL && removableNode->youngestChild != NULL) {
        comp_tree_t* bufferNode = removableNode->youngestChild;
        bufferNode->father = NULL;
        while (bufferNode->olderSibling != NULL) {
            bufferNode = bufferNode->olderSibling;
            bufferNode->father == NULL;
        }
    }
    else if (removableNode->youngestChild == NULL) {
        safelyRemoveNode(removableNode);
    }
    else { // se ele tem filho(s) e tem pai
        safelyRemoveNode(removableNode);
        comp_tree_t* bufferNode = removableNode->youngestChild;
        comp_tree_t* nextNode = bufferNode->olderSibling;
        bufferNode->father = removableNode->father;
        bufferNode->youngerSibling = removableNode->youngerSibling;
        if (bufferNode->youngerSibling != NULL)
            bufferNode->youngerSibling->olderSibling = bufferNode;
        while (nextNode != NULL) {
            bufferNode = nextNode;
            nextNode = nextNode->olderSibling;
            bufferNode->father = removableNode->father;
        }
        bufferNode->olderSibling = removableNode->olderSibling;
        if (bufferNode->olderSibling != NULL)
            bufferNode->olderSibling->youngerSibling = bufferNode;
        if (removableNode->youngerSibling == NULL)
           removableNode->father->youngestChild = removableNode-> youngestChild;
    }
    free(removableNode); 
    return youngestChild;
}

// Aplica a fun��o nativa de libera��o de mem�ria free() recursivamente sobre
// todos os nodos filhos de um dado nodo, al�m do pr�prio.
// Entrada: nodo ter a si e seus filhos desalocados.
void freeNodeAndChildren(comp_tree_t* father) {
     if (father->youngestChild != NULL)
          freeNodeAndChildren(father->youngestChild);          
     if (father->olderSibling != NULL)
          freeNodeAndChildren(father->olderSibling);
     free(father);
}

// Fun��es "private":
void safelyRemoveNode(comp_tree_t* removableNode) {
    if ((removableNode->father != NULL) && (removableNode->olderSibling == NULL && removableNode->youngerSibling == NULL)) {
        removableNode->father->youngestChild = NULL;
    }
    else if ((removableNode->father != NULL) && (removableNode->olderSibling != NULL && removableNode->youngerSibling == NULL)) {
        removableNode->father->youngestChild = removableNode->olderSibling;
        removableNode->olderSibling->youngerSibling = NULL;
    }
    else if ((removableNode->father != NULL) && (removableNode->olderSibling == NULL && removableNode->youngerSibling != NULL)) {
        removableNode->youngerSibling->olderSibling = NULL;
    }
    else if ((removableNode->father != NULL) && (removableNode->olderSibling != NULL && removableNode->youngerSibling != NULL)) {
        removableNode->olderSibling->youngerSibling = removableNode->youngerSibling;   
        removableNode->youngerSibling->olderSibling = removableNode->olderSibling;
    }  
}

void tests_tree () {
    // Teste de appendChild:
    // Estrutura da �rvore:
    //           100
    //       30   20   10
    //       3         1
    comp_tree_t* cem = createNode(100);
    comp_tree_t* dez = createNode(10);
    comp_tree_t* vinte = createNode(20);
    comp_tree_t* trinta = createNode(30);
    comp_tree_t* um = createNode(1);
    comp_tree_t* dois = createNode(2);
    comp_tree_t* tres = createNode(3);        
    appendChild(cem,dez);
    appendChild(cem,vinte);
    appendChild(cem,trinta);
    appendChild(dez,um);
    appendChild(trinta,tres);
    
    printf("Teste de appendChild():\n");
    printf("%d\n",cem->youngestChild->youngestChild->type); // imprime o filho do primeiro filho de 100, que � 3
    printf("%d\n",cem->youngestChild->olderSibling->type); // imprime o segundo filho de 100, que � 20
    printf("%d\n",cem->youngestChild->olderSibling->olderSibling->youngestChild->type); // imprime o filho do terceiro filho de 100, que � 1
    
    // Teste 1 de safelyRemoveNode_DiscardingChildren
    // Estrutura da �rvore ANTES ---- Estrutura da �rvore DEPOIS
    //           100                            100
    //       30   20   10                     30   10
    //       3    2    1                      3    1     
    cem = createNode(100);
    dez = createNode(10);
    vinte = createNode(20);
    trinta = createNode(30);
    um = createNode(1);
    dois = createNode(2);
    tres = createNode(3);
    appendChild(cem,dez);
    appendChild(cem,vinte);
    appendChild(cem,trinta);
    appendChild(dez,um);
    appendChild(vinte,dois);
    appendChild(trinta,tres);

    printf("Teste 1 de safelyRemoveNode_DiscardingChildren():\n");
    printf("%d\n",cem->youngestChild->olderSibling->type); // imprime o segundo filho de 100, que � 20    
    safelyRemoveNode_DiscardingChildren(vinte);
    printf("%d\n",cem->youngestChild->olderSibling->type); // imprime o segundo filho de 100, que agora � 10
    
    // Teste 1 de safelyRemoveNode_AttachingChildrenToFather
    // Estrutura da �rvore ANTES ---- Estrutura da �rvore DEPOIS
    //           100                              100
    //     30    20     10                  30   2   4   10
    //     3    2  4    1                   3            1     
    cem = createNode(100);
    dez = createNode(10);
    vinte = createNode(20);
    trinta = createNode(30);
    um = createNode(1);
    dois = createNode(2);
    tres = createNode(3);
    comp_tree_t* quatro = createNode(4);
    appendChild(cem,dez);
    appendChild(cem,vinte);
    appendChild(cem,trinta);
    appendChild(dez,um);
    appendChild(vinte,quatro);
    appendChild(vinte,dois);
    appendChild(trinta,tres);

    printf("Teste 1 de safelyRemoveNode_AttachingChildrenToFather():\n");
    printf("%d\n",cem->youngestChild->olderSibling->type); // imprime o segundo filho de 100, que � 20    
    printf("%d\n",cem->youngestChild->olderSibling->olderSibling->type); // imprime o terceiro filho de 100, que � 10
    safelyRemoveNode_AttachingChildrenToFather(vinte);
    printf("%d\n",cem->youngestChild->olderSibling->type); // imprime o segundo filho de 100, que agora � 2
    printf("%d\n",cem->youngestChild->olderSibling->olderSibling->type); // imprime o terceiro filho de 100, que agora � 4   
    printf("%d\n",cem->youngestChild->olderSibling->olderSibling->olderSibling->type); // imprime o terceiro quarto de 100, que agora � 10   
    printf("%d\n",dois->father->type); // imprime o pai do dois, que agora � 100
    printf("%d\n",dez->youngerSibling->type); // imprime o irm�o mais novo do 10, que agora � 4
    printf("%d\n",trinta->olderSibling->type); // imprime o irm�o mais velho do 30, que agora � 2
    
    // Teste 2 de safelyRemoveNode_AttachingChildrenToFather
    // Estrutura da �rvore ANTES ---- Estrutura da �rvore DEPOIS
    //           100                           100
    //     30   2   4   10                3   2   4   10
    //     3            1                             1
    printf("Teste 2 de safelyRemoveNode_AttachingChildrenToFather():\n");
    safelyRemoveNode_AttachingChildrenToFather(trinta);
    printf("%d\n",cem->youngestChild->type); // imprime o primeiro filho de 100, que agora � 3  
    printf("%d\n",tres->olderSibling->type); // imprime o irm�o mais velho de 3, que agora � 2
    printf("%d\n",dois->youngerSibling->type); // imprime o irm�o mais novo de 2, que agora � 3
    
    // Teste 2 de safelyRemoveNode_DiscardingChildren
    // Estrutura da �rvore ANTES ---- Estrutura da �rvore DEPOIS
    //              100                         100
    //        3   2   4   10                3    2    4  
    //                    1
    printf("Teste 2 de safelyRemoveNode_DiscardingChildren():\n");
    safelyRemoveNode_DiscardingChildren(dez);

    
    system("pause");
}

// int main () { testes(); }
