%{
#include <stdio.h>
#include <stdlib.h>
#include "comp_tree.h"
#include "iks_ast.h"

%}

/* Declaração dos tokens da gramática da Linguagem K */
/* Palavras Reservadas (PR) */
%token TK_PR_INT         256   /* int       */
%token TK_PR_FLOAT       257   /* float     */
%token TK_PR_BOOL        258   /* bool      */
%token TK_PR_CHAR        259   /* char      */
%token TK_PR_STRING      260   /* string    */
%token TK_PR_IF          261   /* if        */
%token TK_PR_THEN        262   /* then      */
%token TK_PR_ELSE        263   /* else      */
%token TK_PR_WHILE       264   /* while     */
%token TK_PR_DO          265   /* do        */
%token TK_PR_INPUT       266   /* input     */
%token TK_PR_OUTPUT      267   /* output    */
%token TK_PR_RETURN      268   /* return    */

/* Operadores Compostos (OC) */
%token TK_OC_LE    270   /* <=        */
%token TK_OC_GE    271   /* >=        */
%token TK_OC_EQ    272   /* ==        */
%token TK_OC_NE    273   /* !=        */
%token TK_OC_AND   274   /* &&        */
%token TK_OC_OR    275   /* ||        */

/* Literais (LIT) */
%token TK_LIT_INT        281
%token TK_LIT_FLOAT      282
%token TK_LIT_FALSE      283
%token TK_LIT_TRUE       284
%token TK_LIT_CHAR       285
%token TK_LIT_STRING     286

/* Indentificador */
%token TK_IDENTIFICADOR  290

/* Erro */
%token TOKEN_ERRO        291
%%
 /* Regras (e ações) da gramática da Linguagem K */

p : program                             { $$ = $1; /*printTree($$);*/ const void* GVTree = createGVT($$);}

program : global_declaration program   {  $$ = astCreate(IKS_AST_PROGRAMA, $1, $2, NULL, NULL); }
	| function_declaration program     {  $$ = astCreate(IKS_AST_PROGRAMA, $1, $2, NULL, NULL); }
	|
	;

global_declaration : variable_declaration ';'  { $$ = $1 }
	| vector_declaration ';'   { $$ = $1 }
	;

variable_declaration :  type ':' TK_IDENTIFICADOR                  { $$ = astCreate(IKS_AST_IDENTIFICADOR, $1, $3, NULL, NULL); }
	;

vector_declaration : type ':' TK_IDENTIFICADOR '[' TK_LIT_INT ']'  { $$ = astCreate(IKS_AST_IDENTIFICADOR, $1, $3, NULL, NULL); }
	;

type : TK_PR_INT                       { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_PR_FLOAT                      { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_PR_BOOL                       { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_PR_CHAR                       { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_PR_STRING          	       { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	;






function_declaration : header local_declaration command_block { $$ = astCreate(IKS_AST_FUNCAO, $1, $2, $3, NULL); }
	;

header : type ':'  TK_IDENTIFICADOR'(' param_list ')'
	;

param_list : param_list_not_empty
	|
	;

param_list_not_empty : param ',' param_list_not_empty
	| param
	;

param :  type ':' TK_IDENTIFICADOR
	;

local_declaration : variable_declaration ';' local_declaration { $$ = astCreate(IKS_AST_BLOCO, $1, $3, NULL, NULL); }
	|                                       
	;





command_block : '{' seq_command '}'     { $$ = $2 }
	;

seq_command : command                   { $$ = $1 }
	| command ';' seq_command           { $$ = astCreate(IKS_AST_BLOCO, $1, $3, NULL, NULL); }
	;
    
command: command_block                  { $$ = $1 }
	| variable_declaration              { $$ = $1 }
	| vector_declaration                { $$ = $1 }
	| flow_control                      { $$ = $1 }
	| atrib                             { $$ = $1 }
	| input                             { $$ = $1 }
	| output                            { $$ = $1 }
	| return                            { $$ = $1 }
	|
	;

atrib : TK_IDENTIFICADOR '=' expr               { $$ = astCreate(IKS_AST_ATRIBUICAO, $1, $3, NULL, NULL); }
	| TK_IDENTIFICADOR '[' expr ']' '=' expr    { $$ = astCreate(IKS_AST_ATRIBUICAO, $1, $3, $6, 0); }
	;

input : TK_PR_INPUT TK_IDENTIFICADOR  { $$ = astCreate(IKS_AST_INPUT, $1, NULL, NULL, NULL); }
	;

output : TK_PR_OUTPUT expression_list_not_empty { $$ = astCreate(IKS_AST_OUTPUT, $1, NULL, NULL, NULL); }
	;

expression_list_not_empty : expr ',' expression_list_not_empty
	| expr      
	;

flow_control : TK_PR_IF '(' expr ')' TK_PR_THEN command           { $$ = astCreate(IKS_AST_IF_ELSE , $2, $4, NULL, NULL); }
	| TK_PR_IF '(' expr ')' TK_PR_THEN command TK_PR_ELSE command { $$ = astCreate(IKS_AST_IF_ELSE , $2, $4, $6, NULL); }
	| TK_PR_WHILE '(' expr ')' command                            { $$ = astCreate(IKS_AST_WHILE_DO, $2, $3, NULL, NULL); }
	;

return : TK_PR_RETURN expr                     { $$ = astCreate(IKS_AST_RETURN, $1, NULL, NULL, NULL); }
	;

expr : TK_IDENTIFICADOR
	| TK_IDENTIFICADOR '[' expr ']'            { $$ = astCreate(IKS_AST_VETOR_INDEXADO, $1, $3, NULL, NULL); }
	| TK_LIT_INT                               { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_LIT_FLOAT                             { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); } 
	| TK_LIT_FALSE                             { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_LIT_TRUE                              { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_LIT_CHAR                              { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_LIT_STRING                            { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| expr '+' expr                            { $$ = astCreate(IKS_AST_ARIM_SOMA, $1, $3, NULL, NULL); }
	| expr '-' expr                            { $$ = astCreate(IKS_AST_ARIM_SUBTRACAO, $1, $3, NULL, NULL); }
	| expr '*' expr                            { $$ = astCreate(IKS_AST_ARIM_MULTIPLICACAO, $1, $3, NULL, NULL); }
	| expr '/' expr                            { $$ = astCreate(IKS_AST_ARIM_DIVISAO, $1, $3, NULL, NULL); }
	| expr '<' expr                            { $$ = astCreate(IKS_AST_LOGICO_COMP_L, $1, $3, NULL, NULL); }
	| expr '>' expr                            { $$ = astCreate(IKS_AST_LOGICO_COMP_G, $1, $3, NULL, NULL); }
	| '(' expr ')'                             { $$ = $2 }
	| expr TK_OC_LE expr                       { $$ = astCreate(IKS_AST_LOGICO_COMP_LE, $1, $3, NULL, NULL); }
	| expr TK_OC_GE expr                       { $$ = astCreate(IKS_AST_LOGICO_COMP_GE, $1, $3, NULL, NULL); }
	| expr TK_OC_EQ expr                       { $$ = astCreate(IKS_AST_LOGICO_COMP_IGUAL, $1, $3, NULL, NULL); }
	| expr TK_OC_NE expr                       { $$ = astCreate(IKS_AST_LOGICO_COMP_DIF, $1, $3, NULL, NULL); }
	| expr TK_OC_AND expr                      { $$ = astCreate(IKS_AST_LOGICO_E, $1, $3, NULL, NULL); }
	| expr TK_OC_OR expr                       { $$ = astCreate(IKS_AST_LOGICO_OU, $1, $3, NULL, NULL); }
	| TK_IDENTIFICADOR '(' expression_list ')' { $$ = astCreate(IKS_AST_CHAMADA_DE_FUNCAO, $1, $3, NULL, NULL); }
	;

expression_list : expression_list_not_empty
	|
	;

%%
