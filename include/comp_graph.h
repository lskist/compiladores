/* Compiladores, Turma B, Grupo: Andre Luz Gon�alves, Andre Santal� de Oliveira, Lisardo Sallaberry Kist */
typedef struct graph *comp_graph_t;

comp_graph_t graph_create(int n);

void graph_destroy(comp_graph_t);

void graph_add_edge(comp_graph_t, int source, int sink);

comp_graph_t graph_add_node(comp_graph_t g);

int graph_out_degree(comp_graph_t, int source);

int graph_has_edge(comp_graph_t, int source, int sink);

void graph_print(comp_graph_t g);
