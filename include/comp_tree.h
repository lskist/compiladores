/* Compiladores, Turma B, Grupo: Andr� Luz Gon�alves, Andr� Santal� de Oliveira, Lisardo Sallaberry Kist */
#include <stdio.h>
#include <stdlib.h>

// Estrutura de um nodo: pode representar uma raiz, nodo intermedi�rio ou
// folha, dependendo se possui irm�os, filhos ou pai.
typedef struct comp_tree_t comp_tree_t;

struct comp_tree_t {
    comp_tree_t* olderSibling; 
    comp_tree_t* youngerSibling;
    comp_tree_t* youngestChild; 
    comp_tree_t* father;
    int type;
}; 

// Assinaturas das fun��es
comp_tree_t* createNode(int data);
comp_tree_t* astCreate(int type, comp_tree_t* first, comp_tree_t* second, comp_tree_t* third, comp_tree_t* fourth);
void appendChild(comp_tree_t* newFather, comp_tree_t* newChild);
void safelyRemoveNode_DiscardingChildren(comp_tree_t* removableNode);
comp_tree_t* safelyRemoveNode_AttachingChildrenToFather(comp_tree_t* removableNode);
void freeNodeAndChildren(comp_tree_t* father);
void safelyRemoveNode(comp_tree_t* removableNode);
void freeNodeAndChildren(comp_tree_t* father);
